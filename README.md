# Harno-build

Npm module that helps you build your project

## Install
```console
npm install --save-dev harno-build
```

## Usage
Create gulpfile.js in your project
```js
// you should have gulp@4.0.0 in devDependencies
const gulp = require('gulp')
const Build = require('harno-build').default
// here you can overwrite default config values
const config = {
  unitTestsCoverage: {
    thresholds: {
      lines: 90,
      functions: 90,
      branches: 90,
      statements: 90
    }
  }
}

const build = new Build(config)
build.initGulpTasks(gulp)
```

## API
### `new Build(config)`
- config - object with user config values

Creates new build instance with its own configuration

### `build.initGulpTasks(gulp)`
- gulp - instance of gulp@4.0.0

Inits all available gulp tasks

## Default config values
All default config values are located at [app/config.defaults.yml](app/config.defaults.yml)

## Gulp tasks available
All available task and small description to them can be found in gulp default tasks
```bash
 gulp
 # or
 gulp default
 ```

## License
[License](LICENSE)
