import standard from 'standard'
import colors from 'chalk'
import util from 'util'

standard.lintFiles = util.promisify(standard.lintFiles)

/**
 * Class that defines code style tasks
 */
export default class CodeStyle {
  /**
   * Inits service
   */
  init () {
    this._config = this._d.config.c.codeStyle
    this._logger = this._d.logger.createLogger('CodeStyleTask')
  }

  /**
   * Returns code style task function
   * @return {Function} code style task function
   */
  get codeStyle () {
    return this._codeStyle.bind(this)
  }

  /**
   * Checks code style
   * @return {Promise<bool>} true if code style passes
   */
  async _codeStyle () {
    this._logger.trace('Code style task started')
    let wrongCodeStyle = false

    const stat = await standard.lintFiles(this._config.files)

    for (const file of stat.results) {
      if (file.messages.length > 0) {
        wrongCodeStyle = true
        this._logger.error(colors.underline(file.filePath))
      }
      for (const message of file.messages) {
        this._logger.error(
          `  `,
          colors.red(message.severity, ' '),
          `${colors.blueBright(message.line)}:${colors.white(message.column)}  `,
          colors.yellow(message.message, '-'),
          colors.cyan(message.ruleId)
        )
      }
    }

    if (wrongCodeStyle) {
      this._logger.error('Code style task failed')
    } else {
      this._logger.info('Code style check passed successfully')
    }

    return !wrongCodeStyle
  }
}
