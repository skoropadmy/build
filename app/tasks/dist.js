/**
 * Class that defines distribution tasks
 */
export default class Dist {
  /**
   * Inits dist tasks
   */
  init () {
    this._config = this._d.config.c.dist
    this._logger = this._d.logger.createLogger('DistTask')
  }

  /**
   * Returns distribution task function
   * @return {Function} distribution task function
   */
  get dist () {
    return this._dist.bind(this)
  }

  /**
   * Copies compiled files to distribution folder
   * @return {Promise<bool>} relosves with true when files are copied
   */
  async _dist () {
    try {
      this._logger.trace('Dist task started')
      await this._d.compile.compile()
      this._logger.trace(`Moving files from ${this._config.src} to ${this._config.dest}`)
      await this._d.fm.copy(this._config.src, this._config.dest)
      this._logger.info('Dist task finished successfully')
      return true
    } catch (e) {
      this._logger.error('Dist task failed', e)
      return false
    }
  }
}
