import fse from 'fs-extra'

/**
 * Class that defines methods for files management
 */
export default class FilesManagement {
  /**
   * Removes files/folders
   * @param {Array<String>} paths array of paths (files/folders) that would be removed
   * @return {Promise} resolves when all paths removed
   */
  async remove (paths) {
    return Promise.all(paths.map(p => { return fse.remove(p) }))
  }

  /**
   * Copies file/folder from source to destination
   * @param {string} src source folder/file
   * @param {string} dest destination folder/file
   * @return {Promise} resolves when folder/file are copied
   */
  async copy (src, dest) {
    await fse.remove(dest)
    await fse.copy(src, dest)
  }
}
