import colors from 'chalk'
import babel from 'gulp-babel'
import uglify from 'gulp-uglify'
import gzip from 'gulp-gzip'
import gulpif from 'gulp-if'
import through from 'through'
import lazypipe from 'lazypipe'
import bro from 'gulp-bro'
import babelify from 'babelify'

/**
 * Class that defines compile tasks
 */
export default class Compile {
  /**
   * Inits compile tasks
   * @param {Object} gulp gulp instance
   */
  init (gulp) {
    this._gulp = gulp
    this._config = this._d.config.c.compile
    if (typeof this._config.steps === 'string') this._config.steps = this._config.presets[this._config.steps] || []
    this._logger = this._d.logger.createLogger('CompileTask')
  }

  /**
   * Returns compile task function
   * @return {Function} compile task function
   */
  get compile () {
    return this._compile.bind(this)
  }

  /**
   * Compiles an app according to steps in the config
   * @return {Promise<bool>} resolves with true if app is successfully compiled
   */
  async _compile () {
    this._logger.trace('Compile task started')
    for (const [index, { name, src, transforms, dest }] of this._config.steps.entries()) {
      this._logger.trace(`Step ${colors.yellow(index)} started ~ ${colors.magenta(name)}`)

      try {
        await new Promise((resolve, reject) => {
          this._gulp.src(src)
            .pipe(gulpif(transforms.length > 0, this._transforms(transforms)))
            .on('error', reject)
            .pipe(this._gulp.dest(dest))
            .on('error', reject)
            .on('end', resolve)
        })
        this._logger.trace(`Step ${colors.yellow(index)} ${colors.green('finished')} ~ ${colors.magenta(name)}`)
      } catch (e) {
        this._logger.error(`Step ${colors.yellow(index)} ${colors.red('failed with an error')} ~ ${colors.magenta(name)}`, e)
        return false
      }
    }

    this._logger.info('Compile task finished successfully')
    return true
  }

  /**
   * Creates transform channel with different pipes
   * @param {Array<string|Object>} transforms array with different transform options
   * @return {Object} transform channel (stream)
   */
  _transforms (transforms) {
    let transformsChannel = lazypipe()
    const emptyChannel = through()

    for (const transform of transforms) {
      const transformName = typeof transform === 'string' ? transform : transform.name

      if (transformName === 'babel') {
        transformsChannel = transformsChannel.pipe(() => this._babelTransform())
        this._logger.trace(`Babel transform added`)
      } else if (transformName === 'browserify') {
        transformsChannel = transformsChannel.pipe(() => this._browserifyTransform(transform))
        this._logger.trace(`Browserify transform added`)
      } else if (transformName === 'uglify') {
        transformsChannel = transformsChannel.pipe(() => this._uglifyTransform())
        this._logger.trace(`Uglify transform added`)
      } else if (transformName === 'gzip') {
        transformsChannel = transformsChannel.pipe(() => this._gzipTransform())
        this._logger.trace(`Gzip transform added`)
      } else {
        throw new Error(`There is no '${transformName}' transform`)
      }
    }

    // needed because lazypipe cannot create stream if it wasn't piped to anything
    transformsChannel = transformsChannel.pipe(() => emptyChannel)

    return transformsChannel()
  }

  /**
   * Creates babel transform channel
   * @return {object} babel transform channel (stream)
   */
  _babelTransform () {
    return lazypipe().pipe(() => babel())()
  }

  /**
   * Creates browserify transform channel
   * @param {string|Object} transform transform options
   * @return {Object} browserify transform channel (stream)
   */
  _browserifyTransform (transform) {
    const options = { transform: [], error: 'emit' }

    if (typeof transform.browserifyTransforms === 'object') {
      for (const browserifyTransform of transform.browserifyTransforms) {
        if (browserifyTransform === 'babelify') options.transform.push(babelify)
      }
    }

    return lazypipe().pipe(() => bro(options))()
  }

  /**
   * Creates uglify transform channel
   * @return {object} uglify transform channel (stream)
   */
  _uglifyTransform () {
    return lazypipe().pipe(() => uglify())()
  }

  /**
   * Creates gzip transform channel
   * @return {object} gzip transform channel (stream)
   */
  _gzipTransform () {
    return lazypipe().pipe(() => gzip())()
  }
}
