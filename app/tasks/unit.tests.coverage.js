import fs from 'fs'
import { spawn } from 'child_process'

/**
 * Class that defines unit tests coverage tasks
 */
export default class UnitTestsCoverage {
  /**
   * Inits service
   */
  init () {
    this._config = this._d.config.c.unitTestsCoverage
    this._logger = this._d.logger.createLogger('UnitTestsCoverageTask')
  }

  /**
   * Returns unit tests coverage task function
   * @return {Function} unit tests coverage task function
   */
  get unitTestsCoverage () {
    return this._unitTestsCoverage.bind(this)
  }

  /**
   * Creates coverage report and checks thresholds
   * @return {Promise<bool>} resolves with true if thresholds passed
   */
  async _unitTestsCoverage () {
    this._logger.trace('Unit tests coverage task started')

    await new Promise((resolve, reject) => {
      const nyc = spawn(this._config.nycBin, [
        '--require', '@babel/register',
        '--reporter', 'html',
        '--reporter', 'json-summary',
        '--tempDir', `${this._config.dest}.nyc_output`,
        '--reportDir', this._config.dest,
        '--include', this._config.jsFiles,
        this._config.mochaBin, '-b', this._config.testFiles,
        '--color'
      ])

      nyc.stdout.pipe(process.stdout)
      nyc.stderr.pipe(process.stderr)

      nyc.on('error', (err) => {
        reject(err)
      })

      nyc.on('close', (code) => {
        resolve()
      })
    })

    return this._thresholdsSummary()
  }

  /**
   * Outputs coverage thresholds report
   * @return {bool} true if coverage thresholds passed
   */
  _thresholdsSummary () {
    const coverage = JSON.parse(fs.readFileSync(`${this._config.dest}coverage-summary.json`))
    const thresholds = this._config.thresholds

    let unitTestsCoveragePassed = true

    for (const coverageUnit of ['lines', 'branches', 'functions', 'statements']) {
      const threshold = thresholds[coverageUnit]
      let current = coverage.total[coverageUnit].pct
      if (current === 'Unknown') current = 0

      if (current < threshold) {
        this._logger.error(`Bad '${coverageUnit}' coverage: ${current}% (${threshold}%)`)
        unitTestsCoveragePassed = false
      } else {
        this._logger.trace(`Good '${coverageUnit}' coverage: ${current}% (${threshold}%)`)
      }
    }

    if (unitTestsCoveragePassed) {
      this._logger.info('Unit tests coverage task finished successfully')
    } else {
      this._logger.error('Unit tests coverage task failed')
    }

    return unitTestsCoveragePassed
  }
}
