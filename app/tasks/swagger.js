import swaggerParser from 'swagger-parser'
import { spawn } from 'child_process'

/**
 * Class that defines swagger tasks
 */
export default class Swagger {
  /**
   * Inits service
   */
  init () {
    this._config = this._d.config.c.swagger
    this._logger = this._d.logger.createLogger('SwaggerTask')
  }

  /**
   * Returns swagger task function
   * @return {Function} swagger task function
   */
  get swagger () {
    return this._swagger.bind(this)
  }

  /**
   * Validates swagger docs
   * @return {Promise<bool>} true if swagger passed tests
   */
  async _swagger () {
    this._logger.trace('Swagger task started')

    try {
      const docs = await this._getDocs()
      await this._validateDocs(docs)
      return true
    } catch (err) {
      this._logger.error('Swagger validation failed with an error:', err)
      return false
    }
  }

  /**
   * Gets swagger docs from running server
   * @return {Promise<Object>} resolves with swagger docs
   */
  async _getDocs () {
    let docs

    const app = spawn(
      this._config.startServer.command,
      this._config.startServer.args,
      this._config.startServer.options
    )

    try {
      app.stdout.pipe(process.stdout)
      app.stderr.pipe(process.stderr)

      await new Promise(resolve => setTimeout(resolve, this._config.delay))

      const responce = await this._d.request.request(this._config.url)

      docs = JSON.parse(responce)

      app.kill()

      await new Promise((resolve, reject) => {
        app.on('error', (err) => {
          reject(err)
        })

        app.on('close', (code) => {
          resolve()
        })
      })

      return docs
    } catch (e) {
      app.kill()
      throw e
    }
  }

  /**
   * Validates swagger docs
   * @param {Object} docs swagger docs
   * @return {Promise} resolves when validation finishes
   * @throw {Error} in case of validation error
   */
  async _validateDocs (docs) {
    if (!docs) {
      throw new Error('Swagger docs not found, mb server wasn\'t started or some configuration problem')
    }

    await swaggerParser.validate(docs)

    const paths = Object.keys(docs.paths)
    const schemas = Object.keys(docs.definitions)

    const pathsStrings = []
    for (const path of paths) {
      const methods = Object.keys(docs.paths[path]).join(', ').toUpperCase()
      pathsStrings.push(`${methods} ${path}`)
    }

    this._logger.trace(`Paths - ${paths.length}: ${pathsStrings.join(' | ')}`)
    this._logger.trace(`Schemas - ${schemas.length}: ${schemas.join(' | ')}`)

    this._logger.info('Swagger task finished successfully')
  }
}
