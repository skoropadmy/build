import connect from 'gulp-connect'
import lazypipe from 'lazypipe'
import { spawn } from 'child_process'
import colors from 'chalk'
import path from 'path'

/**
 * Class that defines runner tasks
 */
export default class Runner {
  /**
   * Inits runner service
   * @param {Object} gulp gulp instance
   */
  init (gulp) {
    this._gulp = gulp

    this._config = this._d.config.c.runner
    if (typeof this._config.steps === 'string') this._config.steps = this._config.presets[this._config.steps] || []

    this._logger = this._d.logger.createLogger('RunnerTask')

    this._childsToKill = []
    this._webServers = {}
  }

  /**
   * Returns runner task function
   * @return {Function} runner task function
   */
  get runner () {
    return this._runner.bind(this)
  }

  /**
   * Runs the steps in the config. From commands to livereload
   * @return {Promise<bool>} resolves with true when steps are finished
   */
  async _runner () {
    this._logger.trace('Runner task started')

    this._killAllChilds()

    for (const [index, step] of this._config.steps.entries()) {
      const cwd = path.resolve(step.root || './')
      this._logger.trace(`Step ${colors.yellow(index)} started ~ ${colors.magenta(step.name)} ~ ${cwd}`)

      try {
        if (step.tasks) await this._executeTasks(step.tasks, { cwd })
        if (step.exec) await this._executeCommand(step.exec.command, step.exec.args, { wait: step.exec.wait, cwd })
        if (step.web) await this._webLivereload(step.web.host, step.web.port, { cwd })
        this._logger.trace(`Step ${colors.yellow(index)} ${colors.green('finished')} ~ ${colors.magenta(step.name)}`)
      } catch (e) {
        this._logger.error(`Step ${colors.yellow(index)} ${colors.red('failed with an error')} ~ ${colors.magenta(step.name)}`, e)
        return false
      }
    }

    this._logger.info('Runner task finished successfully')
    return true
  }

  /**
   * Executes other gulp tasks
   * @param {Array<string>} tasks array of gulp task names
   * @param {Object} options options
   * @param {string} options.cwd current working directory in which task should be launched
   * @return {Promise} resolves when all tasks are finished
   */
  async _executeTasks (tasks, { cwd }) {
    const currentCwd = process.cwd()
    process.chdir(cwd)

    for (const task of tasks) {
      this._logger.trace(`Task '${task}' started`)

      let taskSuccedded
      if (task === 'dist') {
        taskSuccedded = await this._d.dist.dist()
      } else if (task === 'compile') {
        taskSuccedded = await this._d.compile.compile()
      } else if (task === 'jsdoc') {
        taskSuccedded = await this._d.jsdoc.jsdoc()
      } else if (task === 'code-style') {
        taskSuccedded = await this._d.codeStyle.codeStyle()
      } else if (task === 'unit-tests') {
        taskSuccedded = await this._d.unitTests.unitTests()
      } else if (task === 'unit-tests-coverage') {
        taskSuccedded = await this._d.unitTestsCoverage.unitTestsCoverage()
      } else if (task === 'swagger') {
        taskSuccedded = await this._d.swagger.swagger()
      } else {
        this._logger.error(`Runner doesn't support '${task}' task, ignoring`)
        taskSuccedded = false
      }

      if (taskSuccedded) {
        this._logger.info(`Task '${task}' finished`)
      } else {
        throw new Error(`Task '${task}' failed`)
      }
    }

    process.chdir(currentCwd)
  }

  /**
   * Executes any command
   * @param {string} command command
   * @param {Array<string>} args command arguments
   * @param {Object} options options
   * @param {string} options.cwd current working directory in which command should be executed
   * @param {string} options.wait defines if runner need to wait for this command to finish
   * @return {Promise} resolves when command is finished or right away if options.wait is false
   */
  async _executeCommand (command, args, { wait, cwd }) {
    await new Promise((resolve, reject) => {
      this._logger.trace(`Command '${command} ${args.join(' ')}' executed. Cwd: ${cwd}`)
      const child = spawn(command, args, { cwd })

      child.stdout.pipe(process.stdout)
      child.stderr.pipe(process.stderr)

      child.on('error', (err) => {
        this._logger.error(`Command '${command} ${args.join(' ')}' throws an error`, err)
        reject(err)
      })

      child.on('close', (code) => {
        if (code === null) {
          this._logger.trace(`Command '${command} ${args.join(' ')}' terminated`)
        } else if (code === 0) {
          this._logger.info(colors.green(`Command '${command} ${args.join(' ')}' finished`))
        } else {
          this._logger.error(colors.red(`Command '${command} ${args.join(' ')}' exited with code: ${code}`))
        }

        resolve()
      })

      if (!wait) {
        this._childsToKill.push(child)
        this._logger.info(`Command '${command} ${args.join(' ')}' executed, but will not be waited to finish`)
        resolve()
      }
    })
  }

  /**
   * Kills all child processes which were not killed or finished previously
   */
  _killAllChilds () {
    for (const child of this._childsToKill) {
      child.kill()
    }
    this._childsToKill = []
    this._logger.trace(`All commands, previously started, killed`)
  }

  /**
   * Creates server or reloads it if it is created
   * @param {string} host server host
   * @param {number} port server port
   * @param {Object} options options
   * @param {string} options.cwd current working directory in which command should be executed
   * @return {Promise} resolves when server is created or reloaded
   */
  async _webLivereload (host, port, { cwd }) {
    if (!this._webServers[`${host}:${port}`]) {
      const currentCwd = process.cwd()
      process.chdir(cwd)
      this._logger.info(`Server on ${host}:${port} started`)
      this._webServers[`${host}:${port}`] = connect.server({
        root: './',
        port: port,
        host: host,
        livereload: true
      })
      process.chdir(currentCwd)
    } else {
      const reload = lazypipe().pipe(() => connect.reload())
      await new Promise((resolve, reject) => {
        this._gulp.src(cwd)
          .pipe(reload())
          .on('error', reject)
          .on('end', resolve)
      })
      this._logger.info(`Server on ${host}:${port} reloaded`)
    }
  }
}
