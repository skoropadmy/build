import mocha from 'gulp-mocha'
import glob from 'glob'
import util from 'util'

const globPromisified = util.promisify(glob)

/**
 * Class that defines unit tests tasks
 */
export default class UnitTests {
  /**
   * Inits unit tests tasks
   * @param {Object} gulp gulp instance
   */
  init (gulp) {
    this._gulp = gulp
    this._config = this._d.config.c.unitTests
    this._logger = this._d.logger.createLogger('UnitTestsTask')
  }

  /**
   * Returns unit tests task function
   * @return {Function} unit tests task function
   */
  get unitTests () {
    return this._unitTests.bind(this)
  }

  /**
   * Runs unit tests with mocha
   * @return {Promise<bool>} true if tests passed
   */
  async _unitTests () {
    const files = await globPromisified(this._config.files)
    if (files.length === 0) {
      this._logger.info('No tests found. Unit tests finished successfully')
      return true
    }

    return new Promise(resolve => {
      this._gulp.src(files, { read: false })
        .pipe(mocha({
          require: '@babel/register'
        }))
        .once('error', err => {
          this._logger.error('Unit tests failed with an error:', err.message)
          resolve(false)
        })
        .once('_result', () => {
          this._logger.info('Unit tests finished successfully')
          resolve(true)
        })
    })
  }
}
