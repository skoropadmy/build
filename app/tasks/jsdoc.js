import fs from 'fs'
import colors from 'chalk'
import esdoc from 'esdoc'

/**
 * Class that defines jsdoc tasks
 */
export default class Jsdoc {
  /**
   * Inits service
   */
  init () {
    this._config = this._d.config.c.jsdoc
    this._logger = this._d.logger.createLogger('JsdocTask')
  }

  /**
   * Returns jsdoc task function
   * @return {Function} jsdoc task function
   */
  get jsdoc () {
    return this._jsdoc.bind(this)
  }

  /**
   * Generates js docs and tests them
   * @return {Promise<bool>} true if jsdoc passed tests
   */
  async _jsdoc () {
    this._logger.trace('Jsdoc task started')
    esdoc.generate({
      source: this._config.src,
      destination: this._config.dest,
      plugins: [{
        name: 'esdoc-ecmascript-proposal-plugin',
        option: {
          objectRestSpread: true
        }
      }, {
        name: 'esdoc-standard-plugin',
        option: {
          test: {
            source: this._config.src
          },
          coverage: {
            kind: ['class', 'method', 'get', 'set', 'constructor', 'function']
          }
        }
      }]
    })

    const coverage = this._getJsdocCoverage()
    const lint = this._getJsdocLint()
    const threshold = this._config.threshold

    if (coverage < threshold) {
      this._logger.error(colors.red(`Bad JS docs coverage: ${coverage}%. Threshold: ${threshold}%`))
      return false
    }

    if (lint.length > 0) {
      this._logger.error(colors.red(`JS docs contains errors. Please fix them`))
      return false
    }

    this._logger.info(colors.green(`JS docs coverage: ${coverage}%. Threshold: ${threshold}%`))
    return true
  }

  /**
   * Gets js docs coverage
   * @return {number} js docs coverage
   */
  _getJsdocCoverage () {
    const jsdocCoverage = JSON.parse(fs.readFileSync(`${this._config.dest}coverage.json`))

    return parseFloat(jsdocCoverage.coverage)
  }

  /**
  * Gets js docs lint errors
  * @return {array} array of lint errors
  */
  _getJsdocLint () {
    return JSON.parse(fs.readFileSync(`${this._config.dest}lint.json`))
  }
}
