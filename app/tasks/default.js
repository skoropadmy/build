import colors from 'chalk'

/**
 * Class that defines default task that outputs defined gulp tasks
 */
export default class Default {
  /**
   * Returns default task function
   * @return {Function} default task function
   */
  get default () {
    return this._default.bind(this)
  }

  /**
   * Outputs all tasks descriptions
   */
  async _default () {
    console.log(`This build has this gulp tasks:`)
    this._outputDescription(
      ['test'],
      'task runs clean, code-style, jsdoc and unit-tests-coverage tasks',
      ['plain']
    )
    this._outputDescription(
      ['code-style', 'cs'],
      'task checks JavaScript code with the standard syntax on files with .js extension'
    )
    this._outputDescription(
      ['unit-tests', 'ut'],
      'task runs tests on files with .test.js endings using mocha'
    )
    this._outputDescription(
      ['unit-tests-coverage', 'utc'],
      'task validates tests coverage on files with .test.js endings using nyc'
    )
    this._outputDescription(
      ['swagger', 'sg'],
      'task validates swagger documentation'
    )
    this._outputDescription(
      ['jsdoc', 'j'],
      'task generates and tests js docs with esdoc npm module'
    )
    this._outputDescription(
      ['compile', 'c'],
      'task compiles an app by steps defined in config'
    )
    this._outputDescription(
      ['dist', 'd'],
      'task executes compile task and then copies source to destination'
    )
    this._outputDescription(
      ['runner', 'r'],
      'task executes steps defined in config. It can run livereload server, command or other task'
    )
    this._outputDescription(
      ['explore', 'e'],
      'task opens in the browser and outputs to the console paths(folder/file) specified in config',
      ['plain']
    )
    this._outputDescription(
      ['clean'],
      'task removes all paths(folder/file) specified in config',
      ['plain']
    )

    return true
  }

  /**
   * Outputs tasks description
   * @param {string} taskNames tasks names
   * @param {string} description tasks description
   * @param {Array<string>} [types=['plain','fail-safe','watcher']] types of gulp tasks, that exists
   * @return {string} outputs description
   */
  _outputDescription (taskNames, description, types = ['plain', 'fail-safe', 'watcher']) {
    const mainName = taskNames[0]

    if (types.includes('plain')) {
      const names = `[${taskNames.join(', ')}]`
      console.log(`  ${colors.blueBright(names)} - ${description}`)
    }

    if (types.includes('fail-safe')) {
      const names = `[${taskNames.map(n => `${n}-fail-safe`).join(', ')}]`
      console.log(colors.gray(`  ${names} - task same as ${mainName}, but it doesn't exit with an error code, in case of any error`))
    }

    if (types.includes('watcher')) {
      const names = `[${taskNames.map(n => `${n}-watch`).join(', ')}]`
      console.log(colors.gray(`  ${names} - task watches files and executes ${mainName}-fail-safe task`))
    }
  }
}
