import path from 'path'
import opn from 'opn'

/**
 * Class that defines explore tasks
 */
export default class Explore {
  /**
   * Inits service
   */
  init () {
    this._config = this._d.config.c.explore
    this._logger = this._d.logger.createLogger('ExploreTask')
  }

  /**
   * Returns explore task function
   * @return {Function} explore task function
   */
  get explore () {
    return this._explore.bind(this)
  }

  /**
   * Opens paths in default broswer
   */
  async _explore () {
    this._logger.trace('Explore task started')
    const paths = this._config.paths || []
    let succeeded = true

    for (const p of paths) {
      const absolutePath = path.resolve(process.cwd(), p)
      const url = `file://${absolutePath}`

      try {
        await opn(url)
        this._logger.info(`Path opened: ${url}`)
      } catch (e) {
        this._logger.error(`Cannot open the path: ${url}`)
        succeeded = false
      }
    }

    if (succeeded) {
      this._logger.info('Explore task finished successfully')
    }

    return succeeded
  }
}
