import path from 'path'
import fs from 'fs'

import Kernel from 'harno-kernel'
import RequestService from 'harno-kernel/dist/services/node/request/request.service'
import ErrorHandlerService from 'harno-kernel/dist/services/node/errorHandler/error.handler.service'
import LoggerService from 'harno-kernel/dist/services/node/logger/logger.service'
import ConfigService from 'harno-kernel/dist/services/shared/config/config.service'

import Default from './tasks/default'
import CodeStyle from './tasks/code.style'
import UnitTests from './tasks/unit.tests'
import Compile from './tasks/compile'
import Jsdoc from './tasks/jsdoc'
import UnitTestsCoverage from './tasks/unit.tests.coverage'
import Swagger from './tasks/swagger'
import FilesManagement from './tasks/files.management'
import Dist from './tasks/dist'
import Explore from './tasks/explore'
import Runner from './tasks/runner'

/**
 * Class that contains different tools that will help you to build your application
 */
export default class App {
  /**
   * Registers all services and sets up config
   * @param {Object} config user defined build configuration
   */
  constructor (config) {
    this.kernel = new Kernel()
    this.s = this.kernel.s

    this.kernel.registerService('config', ConfigService)
    this.kernel.registerService('logger', LoggerService)
    this.kernel.registerService('errorHandler', ErrorHandlerService)
    this.kernel.registerService('request', RequestService)

    this.kernel.registerService('default', Default)
    this.kernel.registerService('codeStyle', CodeStyle)
    this.kernel.registerService('unitTests', UnitTests)
    this.kernel.registerService('compile', Compile)
    this.kernel.registerService('jsdoc', Jsdoc)
    this.kernel.registerService('unitTestsCoverage', UnitTestsCoverage)
    this.kernel.registerService('swagger', Swagger)
    this.kernel.registerService('fm', FilesManagement)
    this.kernel.registerService('explore', Explore)
    this.kernel.registerService('dist', Dist)
    this.kernel.registerService('runner', Runner)

    this.kernel.initDependencies({
      request: ['errorHandler'],
      errorHandler: ['logger'],
      codeStyle: ['config', 'logger'],
      unitTests: ['config', 'logger'],
      compile: ['config', 'logger'],
      jsdoc: ['config', 'logger'],
      unitTestsCoverage: ['config', 'logger'],
      swagger: ['config', 'request', 'logger'],
      explore: ['config', 'logger'],
      dist: ['config', 'fm', 'compile', 'logger'],
      runner: ['config', 'dist', 'compile', 'codeStyle', 'unitTests', 'unitTestsCoverage', 'jsdoc', 'swagger', 'logger']
    })

    const defaultConfigFile = fs.readFileSync(path.resolve(__dirname, './config.defaults.yml'))
    this.s.config.setConfig(config)
    this.s.config.setDefaultsYML(defaultConfigFile)
    this.c = this.s.config.c

    this.s.logger.init(this.c.logger)

    this.s.errorHandler.init()
    this.s.dist.init()
    this.s.codeStyle.init()
    this.s.explore.init()
    this.s.jsdoc.init()
    this.s.swagger.init()
    this.s.unitTestsCoverage.init()
  }

  /**
   * Inits all gulp tasks
   * @param {Object} gulp gulp
   */
  initGulpTasks (gulp) {
    this._gulp = gulp

    this.s.unitTests.init(gulp)
    this.s.compile.init(gulp)
    this.s.runner.init(gulp)

    this._registerGulpTasks(['default'], this.s.default.default, { types: ['plain'] })
    this._registerGulpTasks(['code-style', 'cs'], this.s.codeStyle.codeStyle, { watchFiles: this.c.codeStyle.watchFiles })
    this._registerGulpTasks(['unit-tests', 'ut'], this.s.unitTests.unitTests, { watchFiles: this.c.unitTests.watchFiles })
    this._registerGulpTasks(['compile', 'c'], this.s.compile.compile, { watchFiles: this.c.compile.watchFiles })
    this._registerGulpTasks(['jsdoc', 'j'], this.s.jsdoc.jsdoc, { watchFiles: this.c.jsdoc.watchFiles })
    this._registerGulpTasks(['unit-tests-coverage', 'utc'], this.s.unitTestsCoverage.unitTestsCoverage, { watchFiles: this.c.unitTestsCoverage.watchFiles })
    this._registerGulpTasks(['swagger', 'sg'], this.s.swagger.swagger, { watchFiles: this.c.swagger.watchFiles })
    this._registerGulpTasks(['explore', 'e'], this.s.explore.explore, { types: ['plain'] })
    this._registerGulpTasks(['dist', 'd'], this.s.dist.dist, { watchFiles: this.c.dist.watchFiles })
    this._registerGulpTasks(['runner', 'r'], this.s.runner.runner, { watchFiles: this.c.dist.watchFiles })

    gulp.task('clean', async () => {
      await this.s.fm.remove(this.c.clean.paths)
    })

    gulp.task('test', gulp.series('clean', 'code-style', 'jsdoc', 'unit-tests', 'unit-tests-coverage'))
  }

  /**
   * Registers gulp tasks such as just plain task, fail-safe, watchers
   * @param {Array<string>} names task name
   * @param {Function(): boolean} task returns true if task succeeded
   * @param {Object} options register options
   * @param {string} [options.watchFiles=='all files'] files to watch for watcher
   * @param {Array<string>} [options.types=['plain','fail-safe','watcher']] defines types of gulp tasks, that should be created
   */
  _registerGulpTasks (names, task, { watchFiles = '**/*', types = ['plain', 'fail-safe', 'watcher'] }) {
    const mainTask = async () => {
      const passed = await task()
      if (!passed) {
        throw new Error('Task failed')
      }
    }

    if (types.includes('plain')) {
      for (const name of names) {
        this._gulp.task(`${name}`, mainTask)
      }
    }

    if (types.includes('fail-safe')) {
      for (const name of names) {
        this._gulp.task(`${name}-fail-safe`, task)
      }
    }

    if (types.includes('watcher')) {
      for (const name of names) {
        this._gulp.task(`${name}-watch`, async () => {
          console.log(`Watching: ${watchFiles}`)
          await task()
          this._gulp.watch(watchFiles, task)
        })
      }
    }
  }
}
