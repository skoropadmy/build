require('@babel/register')

const gulp = require('gulp')
const Build = require('./app/app').default
const config = {
  unitTestsCoverage: {
    thresholds: {
      lines: 0,
      functions: 0,
      branches: 0,
      statements: 0
    }
  }
}
const build = new Build(config)
build.initGulpTasks(gulp)


// gulp utc-fail-safe && echo $NODE_ENV && ls && ls temp/ && ls temp/coverage/ && cat temp/coverage/coverage-summary.json
