# Changelog
All notable changes to this project will be documented in this file.

## [Unrealesed]
### Changed
- Rename main file to app.js

## [0.0.9] - 2019-01-10
### Added
- Add uglify and gzip transforms to `compile` task
- Add dist-watch, dist-fail-safe tasks
- Add runner task, that runs commands, other tasks or does livereload
- Add new presets (`app`) to runner and compile tasks
### Changed
- Change default `compile.presets.web-server` frontend entry to `start.js` instead of all js files
- Change defautl preset in runner and compile tasks to `app`
- Change `test` task. Now it also runs `unit-tests`
- Change all tasks short names for watchers from `*w` to `*-watcher`
- Improve compile tasks
- Improve dist tasks. Now dist depends on compile tasks

## [0.0.7] - 2018-12-21
### Added
- Add task `dist` that copies src to dest
- Add task `clean` that removes paths (folders/files)
- Add task `explore` that outputs and opens in the browser paths (folders/files)
- Add ecmascript proposals to `jsdocs` tasks
### Changed
- Change default outputs(folders/files) of some tasks changed to temp/ folder (most of them)
- Rename `all` to `test`
- Add `clean` task at the beginning of `test` tasks sequence
- Change `swagger` task logic, introduced new config values
### Fixed
- Skip test files in `compile` task

## [0.0.6] - 2018-12-12
### Fixed
- Path to some executables (nyc, mocha)

## [0.0.5] - 2018-12-12
### Added
- Gulp tasks: compile, unit-tests-coverage, swagger, jsdoc
- Global config with a lot of different values, that can be specified by user or from defaults
### Changed
- Increase required gulp version to 4.0.0
- Improve readme

## [0.0.4] - 2018-12-07
### Added
- Gulp tasks: code-style, unit-tests
